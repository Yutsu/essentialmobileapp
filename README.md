# Essential Mobile Application
![Design homepage of my essential mobile application](./images/home.png)

## Description
This is an application where I share my top 8 essential mobile application that I use every day on my phone. 

## Contains

Carousel.

Click or use arrow of your keyboard to change data.

Display data from an array.

## Technologies Used
Html

Css

Javascript

## Installation
git clone https://gitlab.com/Yutsu/essentialmobileapp.git

## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Cottonbro**

https://www.pexels.com/fr-fr/photo/main-iphone-smartphone-ordinateur-portable-5054355/
