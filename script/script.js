const imageApp = document.querySelector('.img');
const nameApp = document.querySelector('.app-name');
const dateApp = document.querySelector('.app-date');
const descriptionApp = document.querySelector('.app-description');

const btnLeft = document.querySelector('.arrow-left');
const btnRight = document.querySelector('.arrow-right');

let application = reviewApp();
let number = 0;

const displayApp = (currentApp = 0) => {
    const item = application[currentApp];
    imageApp.src = item.logo;
    nameApp.textContent = item.name;
    dateApp.textContent = item.date;
    descriptionApp.textContent = item.description;
}
const displayLeft = () => {
    number--;
    (number < 0) ? number = application.length -1 : displayApp(number);
    displayApp(number);
}
const displayRight = () => {
    number++;
    (number < application.length) ? displayApp(number) : number = 0;
    displayApp(number);
}

const keydown = (e) => {
    if(e.key === "ArrowRight"){
        displayRight();
    }
    if(e.key === "ArrowLeft"){
        displayLeft();
    }
}

const app = () => {
    window.addEventListener("DOMContentLoaded", displayApp());
    document.addEventListener('keydown', keydown);
    btnLeft.addEventListener('click', displayLeft);
    btnRight.addEventListener('click', displayRight);
}
app();
